<h1>Project Reinforcement Learning by Terence NGO & Loïc MARY </h1> 
    

This repository was created for the project of Reinforcement Learning lecture of Ecole Polytechnique.
We created a simple game and implemented a method based on the following [article](https://arxiv.org/pdf/1707.06347.pdf) .

To run the program, open a shell and use the command (at the root of the folder): <br/>
    python(3) main.py 
