from MyGame import MyGame
from MyPolicy import MyPolicy
from EvalUpdate import train_policy
import matplotlib.pyplot as plt
import seaborn as sns



# ------------------------------------------------------------------------------------------------------------------------------------

policy_init = MyPolicy()
updated_policy, losses = train_policy(policy_init=policy_init,
                K=30, lr=0.5, T=20,
                N=1, loss_name="CPI",
                gamma=0.5,
                verbose=True,
                file_name="")

# ------------------------------------------------------------------------------------------------------------------------------------

# let's play with the 2 policies and compare them
game = MyGame()
r1 = game.play(policy_init, T=10)
print(r1["actions"])
print(r1["states"])

print("*"*100)

game.reset()
r2 = game.play(updated_policy, T=20)
print(r2["actions"])
print(r2["states"])
print(r1["actions"]==r2["actions"])





