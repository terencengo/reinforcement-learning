import tensorflow as tf
import numpy as np
from tensorflow.keras.layers import Dense
from tensorflow.keras import Sequential
import matplotlib.pyplot as plt
#np.random.seed(1)

#@title Implementation of the class MyPolicy

def create_nn():
  policy = Sequential()  # report changings in EvalUpdate.py line 56 if you want to modify the structure of the NN 
  # input = current state of the agent = position (x,y) on the grid
  policy.add(Dense(units=10, activation="tanh", input_dim=2, kernel_initializer='glorot_uniform'))
  policy.add(Dense(units=10, activation="tanh", kernel_initializer='glorot_uniform'))
  policy.add(Dense(units=10, activation="tanh", kernel_initializer='glorot_uniform'))
  # 4 possible actions 
  policy.add(Dense(units=4, activation='softmax'))
  return policy

# ---------------------------------------------------------------------------------------------------------

class MyPolicy(object):

  # 10 neurons -> 10 neurons -> 4 neurons (output)
  def __init__(self):
    """
    W1, W2 numpy arrays of size (2, 10) and (10, 4) respectively
    b1, b2 numpy arrays of size 10 and 4 respectively
    """
    policy = create_nn()
    W = policy.get_weights()
    self.policy = policy
    # store previous and current weights/theta for computing r more easily (see later)
    self.current_weights = W 
    self.previous_weights = W 
  
  def get_current_weights(self):
    return self.current_weights

  def get_previous_weights(self):
    return self.previous_weights
  
  def set_weights(self, W):
    """
    W1, W2 numpy arrays of size (2, 10) and (10, 4) respectively
    b1, b2 numpy arrays of size 10 and 4 respectively
    """
    self.policy.set_weights(W)
    self.previous_weights = self.get_current_weights()
    self.current_weights = W
    return self

  def summary(self):
    return self.policy.summary()
  
  def copy(self):
    policy_copy = MyPolicy()
    policy_copy.previous_weights = self.get_previous_weights()
    policy_copy.current_weights = self.get_current_weights()

    policy = create_nn()
    policy.set_weights(self.get_current_weights())
    policy_copy.policy = policy

    return policy_copy


  def proba_a_given_s(self, action_id, state):
    """
    * action_id in {0,1,2,3}
    * state is a list [p_x, p_y] (ex: [1,1], [0,3], ...)

    returns: a TENSOR with the float pi_theta(a|s) 
    """
    state = [state[0]/4, state[1]/4]  # normalization of the vector
    return self.policy(tf.constant([state]))[0][action_id]


  def choose_action_id(self, state):
    """
    state: list of 2 elements (ex: [1,1], [0,3], ...)

    return: 
    a TENSOR with the id (0, 1, 2 or 3) of the selected action (ex. tf.Tensor([1], shape=(1,), dtype=int64))
    """
    # stochastic policy (cannot simply take the argmax every time, otherwise it is deterministic !)
    state = [state[0]/4, state[1]/4] # normalization of the vector
    probas = self.policy(tf.constant([state])).numpy()[0].tolist()
    probas[3] = 1 - (probas[0] + probas[1] + probas[2]) # make sure it exactly sums up to 1
    action_id = np.random.choice(range(4), p=probas)
    return action_id


  def plot_weights_bias(self):
    fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(20,6))
    alpha = 0.7
    N = len(self.get_current_weights())
    # weights
    for i in range(N):
      if i%2==0: # weight
        ax[0].hist(self.get_current_weights()[i], alpha=alpha)
      else: # bias
        ax[1].hist(self.get_current_weights()[i], alpha=alpha)

    ax[0].set_title("Weights")
    ax[1].set_title("Bias")
    plt.show()



