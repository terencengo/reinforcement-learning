import numpy as np
import tensorflow as tf
from tensorflow.keras.layers import Dense
from tensorflow.keras import Sequential
import matplotlib.pyplot as plt
from MyGame import MyGame
# @title Implementation of surrogate functions


# ------------------------------------------------------------------------------------------------------------------------------------


# advantage estimates
# t=0 -> initialization, t=1 -> after the fist move
def A_hat(t, T, game, gamma=0.1):
  """
  t: the current timestep of the game
  T: the duration of the game 
  game: an instance of the class MyGame
  gamma: the smoothing parameter in the discounted reward's formula

  returns : A_hat (float) the value of the estimated advantage function
  """
  S = -game.get_history()["state_values"][t]
  # rewards from t to T-1
  rewards = game.get_history()["rewards"][t:T]
  S += np.sum([gamma**k*reward for k, reward in enumerate(rewards)])
  S += gamma**(T-t)*game.get_history()["state_values"][T]
  return S



# ------------------------------------------------------------------------------------------------------------------------------------



# probablity ratio (of updates)
def r(t, game, policy):
  """
  t: the current timestep of the game
  game: an instance of the class MyGame
  policy: an instance of MyPolicy

  returns: the ratio pi_{theta}(a_t|s_t)/pi_{theta_old}(a_t|s_t)
  """
  action_t, state_t = game.get_history()["actions"][t], game.get_history()["states"][t]
  action_id = game.from_action_to_id(action_t) 

  num = policy.proba_a_given_s(action_id=action_id, state=state_t) # tensor
  # to compute the proba using the old weights, we need to change theta first
  W_previous = policy.get_previous_weights()

  # use a new NN just in case (otherwise, previous weights might be involved in the gradient tape ...)
  previous_policy = policy.copy()
  previous_policy.set_weights(W_previous)
  denom = previous_policy.policy(tf.constant([state_t]))[0][action_id]

  """ policy.set_weights(W1_previous, b1_previous, W2_previous, b2_previous, W3_previous, b3_previous) 
  denom = policy.proba_a_given_s(action_id=action_id, state=state_t)
  # now that we are done with the computation, we reset the current theta
  policy.set_weights(W1, b1, W2, b2, W3, b3) """
  return tf.divide(num, denom)


# ------------------------------------------------------------------------------------------------------------------------------------



# No clipping or penalty
# theta <=> policy 
def L_cpi(game, policy, gamma=0.1):
  """
  game: an instance of the class MyGame
  policy: an instance of MyPolicy
  gamma: the smoothing parameter in the discounted reward's formula

  returns: float the value of the CPI loss (Conservative Policy Iteration)
  """
  T = game.get_t()
  # mean -> expectation (over time t)
  return tf.reduce_mean([r(t, game, policy)*A_hat(t, T, game, gamma) for t in range(T)])


# ------------------------------------------------------------------------------------------------------------------------------------


# clipping
def L_clip(policy_init, gamma=0.3):
  return tf.reduce_mean([tf.minimum(r(t, game, policy_init)*A_hat(t, T, game, gamma),
                                  tf.clip_by_value(r(t, game, policy_init), 1-epsilon, 1+epsilon)*A_hat(t, T, game, gamma)
                                   ) for t in range(T)])

# ------------------------------------------------------------------------------------------------------------------------------------




# ------------------------------------------------------------------------------------------------------------------------------------

# implement a training of the agent with the information specified as parameters
def train_policy(policy_init,
                K=20, lr=0.01, T=20,
                N=1, loss_name="CPI",
                gamma=0.3,
                verbose=True,
                file_name="",
                ):
  """
  * policy_init: an instance of MyPolicy class to train (ie modification of policy.policy.weigths)
  * K: (int) number of training epochs for train the policy
  * lr : (float) learning rate to use when updating the weights
  * T: (int) duration (ie number of moves of the agent) the games we need to play every time
  * N: (int) number of agents that plays in parallel at each epoch (DOESN'T WORK FOR NOW)
  * loss_name: ("CPI", "CLIP" or "KLPEN", default is "CPI") the loss used for the training
  * gamma: (float between 0 and 1 excluded) smoothing parameter for discounted return
  * verbose: (boolean) if True, display some information during the training process and plot a figure at the end of the training
  * file_name: (string) name of the plot. Used iff verbose is set to True

  returns: the updated policy and the losses along the training
  """
  policy = policy_init.copy()
  game = MyGame()
  losses = []
  state_of_interest1 = [0,0] # fixed state under study for probas evol plot
  probas1 = []
  state_of_interest2 = [3,3] # fixed state under study for probas evol plot
  probas2 = []
  chosen_actions = [] # chosen actions (id) from (state_of_interest) over each epoch

  probas1.append([policy.proba_a_given_s(action_id=0, state=state_of_interest1),
                  policy.proba_a_given_s(action_id=1, state=state_of_interest1),
                  policy.proba_a_given_s(action_id=2, state=state_of_interest1),
                  policy.proba_a_given_s(action_id=3, state=state_of_interest1),
                  ])
  probas2.append([policy.proba_a_given_s(action_id=0, state=state_of_interest2),
                  policy.proba_a_given_s(action_id=1, state=state_of_interest2),
                  policy.proba_a_given_s(action_id=2, state=state_of_interest2),
                  policy.proba_a_given_s(action_id=3, state=state_of_interest2),
                  ])

  #game.set_initial_state([3, 3])
  check_init = []

  for k in range(K):
    game.reset() # make sure we start a new game
    # potentially change the initial position of the agent
    #i_init, j_init =  np.random.choice(range(4)), np.random.choice(range(4))
    i = np.random.choice(range(2), p=[0.5, 0.5])
    if i==0:
      check_init.append(0)
      game.set_initial_state([0,0])
    else:
      check_init.append(1)
      game.set_initial_state([3,3])
    with tf.GradientTape() as tape:
      r = game.play(policy, T) # play to get data
      # policy evaluation
      if loss_name=="CPI":
        loss = L_cpi(game, policy, gamma)
      if loss_name=="CLIP":
        loss = L_clip(game, policy, gamma)
   
      
    # gradient
    grad = tape.gradient(loss, policy.policy.trainable_variables)
    #print(tf.reduce_max(grad[0], axis=1)) # is the gradient null ?
    # policy update (gradient ASCENT)
    N = len(policy.get_current_weights())
    W = []
    for i in range(N):
      new_param = policy.get_current_weights()[i] + (lr / np.sqrt(k + 1)) * grad[i]
      W.append(new_param)
      
    policy.set_weights(W)


    # -------- storages and check that policy improves --------
    losses.append(loss) # loss
    print(loss)
    probas1.append([policy.proba_a_given_s(action_id=0, state=state_of_interest1), # policy probas
                  policy.proba_a_given_s(action_id=1, state=state_of_interest1),
                  policy.proba_a_given_s(action_id=2, state=state_of_interest1),
                  policy.proba_a_given_s(action_id=3, state=state_of_interest1),
                  ])
    probas2.append([policy.proba_a_given_s(action_id=0, state=state_of_interest2), # policy probas
                  policy.proba_a_given_s(action_id=1, state=state_of_interest2),
                  policy.proba_a_given_s(action_id=2, state=state_of_interest2),
                  policy.proba_a_given_s(action_id=3, state=state_of_interest2),
                  ])
    first_action = r["actions"][0] # actions 
    chosen_actions.append(game.from_action_to_id(first_action))
  
  # -------- plots --------
  if verbose:
    #plot_evolution_actions(chosen_actions)
    plot_evolution_probas(probas1, state_of_interest1)
    plot_evolution_probas(probas2, state_of_interest2)
    plot_evolution_loss(losses, K, N, gamma, lr, T, file_name)
    #policy.plot_weights_bias()

  #print("Percentage of time the game starts at [3,3]:", np.mean(check_init))
  return policy, losses


# ------------------------------------------------------------------------------------------------------------------------------------

# functions to define plots 

# its name is quite explicit ...
def plot_evolution_loss(loss_name,losses, K, N, lr, T, file_name=""):
  fig = plt.figure(figsize=(10,6))
  plt.plot(losses, color="blue", alpha=0.3)
  plt.xlabel("epoch")
  plt.ylabel("L_"+loss_name)
  plt.title("Evolution of the loss L_{} over {} epochs, with lr={}, N={} and T={}".format(loss_name,K, lr, N, T))
  plt.show()
  if file_name!="":
    fig.savefig(file_name)


# its name is quite explicit as well...
def plot_evolution_probas(probas, state_of_interest):
  K = len(probas)
  fig = plt.figure(figsize=(10,6))
  plt.plot([probas[k][0] for k in range(K)], label="up")
  plt.plot([probas[k][1] for k in range(K)], label="right")
  plt.plot([probas[k][2] for k in range(K)], label="down")
  plt.plot([probas[k][3] for k in range(K)], label="left")
  plt.title("Evol of probas of choosing actions given current state={}".format(state_of_interest))
  plt.legend()
  plt.show()

# its name is quite explicit as well...
def plot_evolution_actions(actions):
  fig = plt.figure(figsize=(10,6))
  plt.plot(actions, "o")
  plt.title("Chosen actions from state=[0,0] over epochs")
  plt.legend()
  plt.show()


