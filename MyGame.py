import numpy as np
import matplotlib.pyplot as plt

def manhattan(a, b):
  return sum(abs(val1 - val2) for val1, val2 in zip(a, b))

#@title Implementation of the class MyGame
class MyGame(object):

  def __init__(self, grid_dim=(5,5), initial_state=[0,0], final_state=[4,4]):
    self.grid_dim = grid_dim
    self.initial_state = initial_state
    self.final_state = final_state

    """ state_values = np.zeros((grid_dim[0], grid_dim[1])) # construction of state_values
    for i in range(grid_dim[0]):
      for j in range(grid_dim[1]):
        state_values[i,j] = 1 / (manhattan([i,j], final_state) + 1) """
    
    state_values = np.array([[1/3, 1/4, 1/4, 1/3, 1/2],
                            [1/4, 1/5, 1/5, 1/4, 1/3],
                            [1/4, 1/5, 1/5, 1/4, 1/3],
                            [1/3, 1/4, 1/4, 1/3, 1/2],
                            [1/2, 1/3, 1/3, 1/2, 1]])

    self.state_values = state_values 
    self.current_state = initial_state
    self.t = 0
    # [i,j] => [y,x] (be careful!)
    self.actions_id_to_vectors = {0: [-1,0], # up
                    1: [0,1], # right
                    2: [1,0], # down
                    3: [0,-1]} # left

    # dictionnary of keys: states, actions, rewards, state_values
    x, y = self.get_current_state()[0], self.get_current_state()[1] 
    self.history = {"states": [self.get_initial_state()], "actions": [], "rewards": [], "state_values": [self.get_state_values()[x,y]]}

  # getters 
  def get_t(self):
    return self.t
  
  def get_current_state(self):
    return self.current_state
  
  def get_initial_state(self):
    return self.initial_state
  
  def get_state_values(self):
    return self.state_values
  
  def get_history(self):
    return self.history
  
  def get_final_state(self):
    return self.final_state

  def get_grid_dim(self):
    return self.grid_dim

  def get_actions_id_to_vectors(self):
    return self.actions_id_to_vectors

  # setters

  def set_state_values(self, new_state_values):
    self.state_values = new_state_values
    #return self
  
  def set_final_state(self, new_final_state):
    self.final_state = new_final_state 
    #return self

  def set_initial_state(self, new_initial_state):
    self.initial_state = new_initial_state 
    #return self
  
  def is_over(self):
    return self.get_current_state() == self.get_final_state()
  
  def reset(self):
    self.current_state = self.get_initial_state()
    x = self.get_current_state()[0]
    y = self.get_current_state()[1]
    self.history = {"states": [self.get_initial_state()], "actions": [], "rewards": [], "state_values": [self.get_state_values()[x, y]]}
    self.t = 0
    return self
  

  def display(self):
    figure, ax = plt.subplots(nrows=1, ncols=2, figsize=(20,6))

    nb_rewards = len(self.history["rewards"])
    nb_states = len(self.history["state_values"])
    ax[0].plot(range(1, nb_rewards + 1), self.history["rewards"], color='blue', alpha=0.7, label="rewards")
    ax[1].plot(range(nb_states), self.history["state_values"], color='red', alpha=0.7, label="state values")
    ax[0].set_xlabel("time")
    ax[1].set_xlabel("time")
    ax[0].set_xticks(range(1, nb_rewards+1))
    ax[1].set_xticks(range(1, nb_states))
    ax[0].legend()
    ax[1].legend()
    plt.show()

  def from_id_to_action(self, action_id):
    """
    action_id : the id of the action 0, 1, 2 or 3

    returns : action list of 2 elements (ex: [1,1], [0,3], ...)
    """
    return self.get_actions_id_to_vectors()[action_id]

  def from_action_to_id(self, action):
    """
    action : action list of 2 elements (ex: [1,1], [0,3], ...)
    returns : the id of the action 0, 1, 2 or 3
    """
    if action == [-1,0]:
        return 0
    if action == [0,1]:
        return 1
    if action == [1,0]:
        return 2
    if action == [0,-1]:
        return 3


  def reward(self, state, action_id):
    """
    state: list of 2 elements (ex: [1,1], [0,3], ...)
    action: the id (0, 1, 2 or 3) of the selected action 

    returns: the reward (int) from the env when the agent takes action while being in state s
    """
    # relevant for dealing with grids
    
    """ d_old = manhattan(state, self.get_final_state())
    action = self.actions_id_to_vectors[action_id]
    new_state = [state[0] + action[0], state[1] + action[1]]
    d_new = manhattan(new_state, self.get_final_state())
    # d_old > d_new <=> we are closer to the destination <=> positive reward
    return (d_old - d_new) """
    v_old = self.get_state_values()[state[0], state[1]]
    action = self.actions_id_to_vectors[action_id]
    new_state = [state[0] + action[0], state[1] + action[1]]
    # manage borders:
    if new_state[0] == -1:
      new_state[0] = self.get_grid_dim()[0] - 1
    
    if new_state[0] == self.get_grid_dim()[0]:
      new_state[0] = 0

    if new_state[1] == -1:
      new_state[1] = self.get_grid_dim()[1] - 1
    
    if new_state[1] == self.get_grid_dim()[1]:
      new_state[1] = 0

    v_new = self.get_state_values()[new_state[0], new_state[1]]
    return 2*(v_new - v_old)

  
  def play(self, policy, T=1):
    """
    policy : an instance of the class MyPolicy
    T : number of moves the agent will do

    returns : a dict of triplets (state, action, reward) collected over time, since the beginning  
    of the game (reset the game to make sure this function returns collections of size T)
    """
    for t in range(T):
      action_id = policy.choose_action_id(self.get_current_state())
      action = self.from_id_to_action(int(action_id))

      reward = self.reward(self.get_current_state(), int(action_id))
      new_state = [self.get_current_state()[0] + action[0], self.get_current_state()[1] + action[1]]

      # manage borders:
      if new_state[0] == -1:
        new_state[0] = self.get_grid_dim()[0] - 1
      
      if new_state[0] == self.get_grid_dim()[0]:
        new_state[0] = 0

      if new_state[1] == -1:
        new_state[1] = self.get_grid_dim()[1] - 1
      
      if new_state[1] == self.get_grid_dim()[1]:
        new_state[1] = 0
    
      new_x = new_state[0]
      new_y = new_state[1]
      self.current_state = new_state
      self.t = self.get_t() + 1

      self.history["states"].append(self.get_current_state())
      self.history["actions"].append(action)
      self.history["state_values"].append(self.get_state_values()[new_x, new_y])
      self.history["rewards"].append(reward)
      
      if self.is_over():
        print("Well done! You won after {} moves".format(t+1))
        break

    return self.history
